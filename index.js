#!/usr/bin/env node

const process = require('process')

const parseArgs = require('minimist')
const request = require('request')

const argv = parseArgs(process.argv.slice(2))

if (argv._.length !== 1) {
  console.error('Usage: ./index.js \'URL\' > output.json')
  process.exit(1)
}
const path = argv._[0]

function handleNode(node) {
  const type = typeof node
  if (type === 'object' && node != null) {
      Object.keys(node).forEach(key => {
        node[key] = handleNode(node[key])
      })
  } else if (type === 'string') {
    try {
      node = JSON.parse(node)
    } catch(error) {}
  }
  return node
}

request
  .get(path, function(req, res, body) {
    const data = JSON.parse(body)
    const transformed = handleNode(data)
    process.stdout.write(JSON.stringify(transformed, null, 4) + '\n')
  })
  .on('error', function(error) {
    process.stderr.write(error.message)
  })
