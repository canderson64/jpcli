# jpcli

Retrieve and pretty print a remote GET endpoint with the added benefit of parsing any inline properties which are stringified JSON.

## Installation

Clone the repo and then run `npm i`.

## Example

```json
{"prop0":"value","prop1":"value","prop2": "{\"sProp\"\:\"sValue\"}"}
```
becomes:
```json
{
    "prop0": "value",
    "prop1": "value",
    "prop2": {
        "sProp": "sValue"
    }
}
```

## Usage

```sh
./index.js 'http://example.com/endpoint?query=value'
```

Or to save to a file:

```sh
./index.js 'http://example.com/endpoint?query=value' > ./output.json
```
